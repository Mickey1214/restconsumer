import { LitElement, html, css } from 'lit-element';

class TestXmlhttp  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: {type: Object}
    };
  }

  constructor() {
    super();
    this.cargarPlaneta();
  }

  render() {
    return html`
      <p><code>${this.planet}</code></p>
    `;
  }

  cargarPlaneta(){
      //xmlHTTPRequest
      var req = new XMLHttpRequest();
      req.open('GET', "https://swapi.dev/api/planets/1", true); 
      req.onreadystatechange = ((event) => {
        if(req.readyState === 4){

            //Si ya termino y a funcionado
            if(req.status === 200){
                this.planet = req.responseText;
                this.requestUpdate();
            }else{
                alert("Error llamando rest");
            }
        }
      });
      req.send(null);
  }
}

customElements.define('test-xmlhttp', TestXmlhttp);