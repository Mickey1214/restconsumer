import { LitElement, html, css } from 'lit-element';

class ProductLists  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos: {type: Array}
    };
  }

  constructor() {
    super();
    this.productos = [];
    this.productos.push({
        "nombre": "Movil XL",
        "descripcion": "Un telefono grande con una de las mejores pantallas."
    });
    this.productos.push({
        "nombre": "Movil Mini",
        "descripcion": "Un telefono mediano con una de las mejores camaras."
    });
    this.productos.push({
        "nombre": "Movil Standard",
        "descripcion": "Un telefono estandar. Nada especial."
    });
  }

  render() {
    return html`
      <div class="container">
        ${this.productos.map(producto => 
            html`<div class="product"><h3>${producto.nombre}</h3><p>${producto.descripcion}</p></div>`)}
      </div>
    `;
  }

  //Regresa todo el shadow root, lo cual no le afecta aqui los estilos 
  createRenderRoot(){
      return this;
  }
}

customElements.define('product-lists', ProductLists);