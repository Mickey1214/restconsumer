const {When, Then, After} = require('@cucumber/cucumber');
const assert = require('assert');
//Selenium driver
const {Builder, By, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');

When("we request the products list", async function(){
    //Instruccion que declara a la variable chrome para que utilice el chromedriver 
    //y para eso se necesita una instancia con servicebuilder 
    chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());
    this.driver = new Builder()
        .forBrowser('chrome')
        .build();

        //El driver se espera a que el elemento (prodcut-list) se cargue
    this.driver.wait(until.elementLocated(By.className('product-list')));
    await this.driver.get("http:://localhost:8000");
});